FROM ruby:3.1.2

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN apt-get update -yqq
RUN apt-get install -yqq --no-install-recommends nodejs
RUN bundle install

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
