Rails.application.routes.draw do
  resources :users, except: %i[destroy]

  resources :todos, except: %i[show new]

  resource :session, only: %i[new create destroy]

  resource :password_reset, only: %i[new create edit update]

  root to: "todos#index"
end
