require 'rails_helper'

RSpec.describe PasswordResetsController, :type => :request do

  let(:user) {User.create(name: "Test", email: "test-reset-password@test.com",
                          password: "123456", password_confirmation: "123456", password_reset_token: "blabla",
                          password_reset_token_sent_at: Time.current)}

  describe "create action" do
    it "redirects to new session path" do
      post "/password_reset"
      expect(response).to redirect_to new_session_path
    end
  end

  describe "update action" do
    it "redirects to new session path" do
      patch "/password_reset", params: {user: {email: user.email, password_reset_token: user.password_reset_token,
                                        password: "654321", password_confirmation: "654321"}}
      expect(response).to redirect_to new_session_path
    end
  end
end
