require 'rails_helper'

RSpec.describe TodosController, :type => :request do
  before do
    login(user)
  end

  let(:user) {User.create(name: "Merhaba", email: "test@test.com", password: "123456", password_confirmation: "123456")}
  let(:test_todo) {Todo.create(title: "test-title", status: false, user_id: 1)}
  def login(user)
    post "/session", params: {session: { email: user.email, password: user.password }}
  end

  describe "create action" do
    it "redirects to root path" do
      post "/todos", params: {todo: {title: "test-title"}}
      expect(response).to redirect_to root_path
    end

    it "render index view if validation fails" do
      post "/todos", params: {todo: {title: ""}}
      expect(response).to render_template(:index)
    end
  end

  describe "index action" do
    it "redirects to index view" do
      get "/todos"
      expect(response).to render_template(:index)
    end
  end


  describe "update action" do
    it "redirects to root path" do
      patch "/todos/#{test_todo.id}", params: {todo: {title: "test"}}
      expect(response).to redirect_to root_path
    end

    it "renders edit template if validation fails" do
      patch "/todos/#{test_todo.id}", params: {todo: {title: ""}}
      expect(response).to render_template(:edit)
    end
  end

  describe "destroy action" do
    it "redirects after destroy" do
    delete "/todos/#{test_todo.id}", params: {id: test_todo.id}
    expect(response).to redirect_to root_path
    end
  end
end
