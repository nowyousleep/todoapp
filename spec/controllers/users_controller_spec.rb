require 'rails_helper'

RSpec.describe UsersController, :type => :request do

  describe "create action" do
    it "redirects to new session path" do
      post "/users", params: {user: {name: "Merhaba", email: "test-user@test.com", password: "123456", password_confirmation: "123456"}}
      expect(response).to redirect_to new_session_path
    end

    it "render new template if validation fails" do
      post "/users", params: {user: {name: "Merhaba", email: "test-user@test.com", password: "123456", password_confirmation: "654321"}}
      expect(response).to render_template(:new)
    end
  end

  describe "show and update action" do
    before do
      login(user)
    end

    let(:user) {User.create(name: "Test", email: "test-user@test.com", password: "123456", password_confirmation: "123456")}
    def login(user)
      post "/session", params: {session: { email: user.email, password: user.password }}
    end

    it "renders show template" do
      get "/users/#{user.id}", params: {id: user.id}
      expect(response).to render_template(:show)
    end

    it "redirect to user path" do
      patch "/users/#{user.id}", params: {user: {name: "New-test-name"}}
      expect(response).to redirect_to user_path(assigns(:user))
    end

    it "renders show edit tamplate if validation fails" do
      patch "/users/#{user.id}", params: {user: {password: "123"}}
      expect(response).to render_template(:edit)
    end
  end
end
