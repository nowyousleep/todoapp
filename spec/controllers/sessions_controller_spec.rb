require 'rails_helper'

RSpec.describe SessionsController, :type => :request do

  let(:user) {User.create(name: "Test", email: "test-session@test.com", password: "123456", password_confirmation: "123456")}

  describe "create action" do
    it "redirects to root path" do
      post "/session", params: {session: {email: "test-session@test.com", password: "123456"}}
      expect(response).to redirect_to root_path
    end

    it "render new template if validation fails" do
      post "/users", params: {user: {email: "test-session@test.com", password: "12345"}}
      expect(response).to render_template(:new)
    end
  end

  describe "destroy action" do
    before do
      login(user)
    end

    def login(user)
      post "/session", params: {session: { email: user.email, password: user.password }}
    end

    it "redirects to new session path" do
      delete "/session"
      expect(response).to redirect_to new_session_path
    end
  end
end
