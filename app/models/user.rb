class User < ApplicationRecord
  include Recoverable

  attr_accessor :old_password, :skip_old_password

  has_many :todos, dependent: :destroy

  has_secure_password validations: false

  validate :password_presence
  validates :password, confirmation: true, allow_blank: true,
                       length: { minimum: 5, maximum: 50 }
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true, 'valid_email_2/email': true
  validate :correct_old_password, on: :update, if: -> { password.present? && !skip_old_password }

  private
  def password_presence
    errors.add(:password, :blank) if password_digest.blank?
  end

  def correct_old_password
    return if BCrypt::Password.new(password_digest_was).is_password?(old_password)

    errors.add(:old_password, 'wrong')
  end
end
