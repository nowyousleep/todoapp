class ApplicationController < ActionController::Base
  include Authentication
  include Cookies
  include Pagy::Backend
end
