class SessionsController < ApplicationController
  before_action :require_no_authentication, only: %i[new create]
  before_action :verify_token, only: :destroy

  def new
  end

  def create
    session_params = params.require(:session)
    user = User.find_by(email: session_params[:email])&.authenticate(session_params[:password])
    if user.present?
      access_token = encode_token(user_id: user.id, exp: 5.minutes.from_now.to_i)
      add_jwt_to_cookies(access_token)
      refresh_token = encode_token(access_token: access_token.slice(-5..-1), exp: 1.month.from_now.to_i)
      add_refresh_token_to_cookies(refresh_token)
      session[:user_id] = user.id
      redirect_to root_path, notice: "You have logged in!"
    else
      flash.now[:alert] = "Incorrect email or password"
      render :new
    end
  end

  def destroy
    session.delete(:user_id)
    flash[:notice] = "You have logged out!"
    redirect_to new_session_path
  end
end
