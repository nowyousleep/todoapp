class TodosController < ApplicationController
  before_action :set_todo, only: %i[edit update destroy]
  before_action :verify_token

  def index
    @pagy, @todos = pagy(Todo.where(user_id: current_user.id).order('status', updated_at: :desc), items: 5)
    @todo = Todo.new
  end

  def edit
  end

  def create
    @todo = Todo.new(todo_params)
    if @todo.save
      redirect_to root_path, notice: "To do has been created!"
    else
      @pagy, @todos = pagy(Todo.where(user_id: current_user.id).order('status', updated_at: :desc), items: 5)
      render :index
    end
  end

  def update
    if @todo.update(todo_params)
      redirect_to root_path, notice: "Changes has been saved!"
    else
      render :edit
    end
  end

  def destroy
    @todo.destroy
    redirect_to root_path, notice: "To do has been deleted!"
  end

  private

  def set_todo
    @todo = Todo.find(params[:id])
  end

  def todo_params
    params.require(:todo).permit(:title, :status).merge(user_id: current_user.id)
  end
end
