class PasswordResetsController < ApplicationController
  before_action :require_no_authentication
  before_action :set_user, only: %i[edit update]

  def create
    @user = User.find_by(email: params[:email])
    if @user.present?
      @user.set_password_reset_token
      PasswordResetMailer.with(user: @user).reset_email.deliver_later
    end
    redirect_to new_session_path, notice: "Password reset instructions have been sent to your email"
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to new_session_path, notice: "Your password has been updated!"
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation).merge(skip_old_password: true)
  end

  def set_user
    @user = User.find_by(email: params[:user][:email],
                          password_reset_token: params[:user][:password_reset_token])
    redirect_to(new_session_path, alert: "Could not reset password") unless @user&.password_reset_perion_valid?
  end
end
