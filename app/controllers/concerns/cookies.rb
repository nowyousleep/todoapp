module Cookies
  extend ActiveSupport::Concern

  included do

    def add_jwt_to_cookies(access_token)
      cookies[:jwt] = {
        value: access_token,
        expires: 1.month.from_now,
        httponly: true,
      }
    end

    def add_refresh_token_to_cookies(refresh_token)
      cookies[:refresh_token] = {
        value: refresh_token,
        expires: 1.month.from_now,
        httponly: true,
      }
    end
  end
end
