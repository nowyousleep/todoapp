module Authentication
  extend ActiveSupport::Concern

  included do

    helper_method :current_user

    private
    def require_no_authentication
      return if current_user.blank?

      flash[:alert] = "You are already log in"
      redirect_to root_path
    end

    def verify_token
      access_token = request.cookies['jwt']
      if access_token
        # Проверяем токен и извлекаем информацию о пользователе
        decoded_access_token = decode_token(access_token)
        if decoded_access_token
          # Если токен просрочится меньше чем через 5 минут - обновим его, используя refresh-токен,
          # а так же обновим сам refresh-токен
          if decoded_access_token[0]['exp'].to_i - Time.now.to_i < 300
            refresh_token = request.cookies['refresh_token']
            decoded_refresh_token = decode_token(refresh_token)
            # Проверяем валидность refresh-токена
            if decoded_refresh_token && decoded_refresh_token[0]['access_token'] == access_token.slice(-5..-1)
              new_access_token = encode_token(user_id: current_user.id, exp: 1.minutes.from_now.to_i)
              add_jwt_to_cookies(new_access_token)
              new_refresh_token = encode_token(access_token: new_access_token.slice(-5..-1), exp: 10.minutes.from_now.to_i)
              add_refresh_token_to_cookies(new_refresh_token)
              return
            # Если refresh-токен невалидный, возвращаем ошибку аутентификации
            else
              session.delete(:user_id)
              flash[:alert] = "You have to log in"
              redirect_to new_session_path
            end
          end
          user_id = decoded_access_token[0]['user_id']
          user = User.find_by(id: user_id)
          # Если пользователь не найден возвращаем ошибку аутентификации
          unless user
            session.delete(:user_id)
            flash[:alert] = "You have to log in"
            redirect_to new_session_path
          end
        # Если срок действия токена истёк возвращаем ошибку
        else
          session.delete(:user_id)
          flash[:alert] = "Session timed out"
          redirect_to new_session_path
        end
      else
        # Если токен отсутствует возвращаем ошибку аутентификации
        session.delete(:user_id)
        flash[:alert] = "You have to log in"
        redirect_to new_session_path
      end
    end

    def current_user
      @current_user ||= User.find_by(id: session[:user_id]) if session[:user_id]
    end

    def encode_token(payload)
      JWT.encode(payload, Rails.application.secrets.secret_key_base)
    end

    def decode_token(token)
      JWT.decode(token, Rails.application.secrets.secret_key_base)
    rescue JWT::DecodeError
      nil
    end
  end
end
