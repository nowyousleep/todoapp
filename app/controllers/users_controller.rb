class UsersController < ApplicationController
  before_action :require_no_authentication, only: %i[new create]
  before_action :verify_token, only: %i[edit update show]
  before_action :set_user, only: %i[edit update]
  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to new_session_path, notice: "You have successfully registered!"
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      redirect_to user_path(@user), notice: "Changes has been saved!"
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :nickname, :email, :password, :password_confirmation, :old_password)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
