class ApplicationMailer < ActionMailer::Base
  default from: "admin@todo_test.com"
  layout "mailer"
end
